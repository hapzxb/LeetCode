package com.hapzxb.leetcode.array;

import java.util.Arrays;

/**
 * @description: 合并两个有序数组
 * ***给定两个有序整数数组*** nums1 和 nums2，将 nums2 合并到 nums1 中，使得 num1 成为一个有序数组。
 * <p>
 * 说明:
 * 初始化 nums1 和 nums2 的元素数量分别为 m 和 n。
 * 你可以假设 nums1 有足够的空间（空间大小大于或等于 m + n）来保存 nums2 中的元素。
 * 示例:
 * <p>
 * 输入:
 * nums1 = [1,2,3,0,0,0], m = 3
 * nums2 = [2,5,6],       n = 3
 * <p>
 * 输出: [1,2,2,3,5,6]
 * @author: zxb
 * @create: 2019-03-18 13:13
 **/
public class Solution {
    public static void main(String[] args) {
        int[] nums1 = new int[]{1, 2, 3, 4, 0, 0, 0};
        int[] nums2 = new int[]{5, 6, 7};
        //merge(nums1, 4, nums2, 3);
        merge2(nums1, 4, nums2, 3);

    }

    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int[] newArray = nums1;
        for (int i = 0; i < nums1.length; i++) {
            newArray[i] = nums1[i];
        }
        for (int i = 0; i < nums2.length; i++) {
            newArray[m + i] = nums2[i];
        }
        nums1 = newArray;
        Arrays.sort(nums1);

    }

    /**
     * 用时最少
     */
    public static void merge2(int[] nums1, int m, int[] nums2, int n) {
        // 最后一个元素的索引
        int index = m + n - 1;
        // A 数组中最后一个元素索引
        int indexA = m - 1;
        // B 数组中最后一个元素索引
        int indexB = n - 1;
        for (int i = index; i >= 0; i--) {
            // A数组中最后一个元素索引 或 B数组中最后一个元素索引 等于-1，跳出循环
            if (indexA == -1 || indexB == -1) {
                break;
            }
            // 如果A数组最后一个元素 大于 B数组最后一个元素
            if (nums1[indexA] > nums2[indexB]) {
                // 把A数组(A[m-1])最后一个元素赋值给A数组A[m+n-1]最后一个元素
                nums1[i] = nums1[indexA];
                // A数组最后一个索引 减1
                indexA--;
            } else {
                // 否则把B数组最后一个元素赋值给A数组最后一个元素
                nums1[i] = nums2[indexB];
                // B数组最后一个索引 减1
                indexB--;
            }
        }
        // 如果A数组中元素都大于B数组中的元素，执行下面代码
        while (indexB >= 0) {
            nums1[indexB] = nums2[indexB];
            indexB--;
        }

    }

}
