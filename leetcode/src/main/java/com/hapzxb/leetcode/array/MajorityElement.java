package com.hapzxb.leetcode.array;

import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * @program: LeetCode
 * @description: 题目：求众数
 * 给定一个大小为 n 的数组，找到其中的众数。众数是指在数组中出现次数大于 ⌊ n/2 ⌋ 的元素。
 * 你可以假设数组是非空的，并且给定的数组总是存在众数。
 * <p>
 * 示例 1:
 * 输入: [3,2,3]
 * 输出: 3
 * 示例 2:
 * <p>
 * 输入: [2,2,1,1,1,2,2]
 * 输出:2
 * @author: zxb
 * @create: 2019-03-17 11:01
 **/
public class MajorityElement {

    @Test
    public void test() {
        Assert.assertEquals(1, majorityElement2(new int[]{2, 2, 1, 1,1, 2, 2}));
    }

    public int majorityElement(int[] nums) {
        //  [n/2] 元素
        int majorityElement = nums.length / 2;
        // 创建一个集合
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            // 思路：数组元素为key，出现次数为value。如果存在key，value+1,如果不存在key，第一次设置value 为1
            map.put(num, map.get(num) != null ? map.get(num).intValue() + 1 : 1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            // 出现次数和[n/2] 进行比较，大于，则返回（其实应该用一个集合来保存所有满足条件的众数，然后再返回）
            if (entry.getValue() > majorityElement) {
                return entry.getKey();
            }
        }
        return 0;
    }


    /**
     * 时间最快
     *
     * @param nums
     * @return
     */
    public int majorityElement2(int[] nums) {
        int result = nums[0], count = 0;
        for (int i = 0; i < nums.length; i++) {
            if (count == 0) {
                result = nums[i];
                count++;
            } else if (result == nums[i]) {
                count++;
            } else {
                count--;
            }
        }
        return result;
    }


}
