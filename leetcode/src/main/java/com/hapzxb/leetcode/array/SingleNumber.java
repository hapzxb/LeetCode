package com.hapzxb.leetcode.array;

import org.junit.Assert;
import org.junit.Test;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * 题目：
 * 给定一个非空整数数组，除了某个元素只出现一次以外，其余每个元素均出现两次。找出那个只出现了一次的元素。
 * 说明：
 * 你的算法应该具有线性时间复杂度。 你可以不使用额外空间来实现吗？
 * <p>
 * 示例 1:
 * 输入: [2,2,1]
 * 输出: 1
 * 示例 2:
 * <p>
 * 输入: [4,1,2,1,2]
 * 输出: 4
 */
public class SingleNumber {

    @Test
    public void test1() {
        Assert.assertEquals(3, singleNumber(new int[]{1, 3, 1, 2, 4, 2, 4}));
        Assert.assertEquals(3, singleNumber2(new int[]{1, 4, 2, 3, 2, 4, 1}));
        Assert.assertEquals(3, singleNumber3(new int[]{1, 4, 2, 3, 2, 4, 1}));
    }


    public int singleNumber(int[] nums) {
        Map<Integer, Integer> map = new HashMap();
        for (int num : nums) {
            map.put(num, map.containsKey(num) ? 0 : 1);
        }
        for (Map.Entry<Integer, Integer> entry : map.entrySet()) {
            if (entry.getValue() == 1) {
                return entry.getKey();
            }
        }
        return -1;
    }

    public int singleNumber2(int[] nums) {
        Arrays.sort(nums);

        for (int i = 0; i < nums.length ; i = i + 2) {
            if (i + 1 >= nums.length ) {
                return nums[i];
            }
            if (nums[i] != nums[i + 1]) {
                return nums[i];
            }
        }

        return -1;
    }

    public int singleNumber3(int[] nums) {
        Arrays.sort(nums);
        int res = 0;
        for (int num : nums) {
            res ^= num;
        }
        return res;

    }

}
